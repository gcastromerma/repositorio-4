﻿using CodFirstBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

using System.Data.Entity;

namespace CodFirstBlog.Services
{
    public class BlogPostRepository
    {
        public List<BlogPostModel> ObtenerTodos()
        {
            using (BlogContext db = new BlogContext())
            {
                List<BlogPostModel> list = db.blogPost
                    .Include(s => s.comentarios)
                    .ToList();
                return list;
            }
        }

        internal void crearBlog(BlogPostModel obj)
        {

            using (BlogContext db = new Models.BlogContext())
            {
                db.blogPost.Add(obj);
                db.SaveChanges();
                db.Dispose();
            }
        }
    }
}