﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;

namespace CodFirstBlog.Models
{
    public class BlogContext:DbContext
    {
        public BlogContext():base("cnn")
        {


        }
        public DbSet<BlogPostModel> blogPost { get; set; }
        public DbSet<ComentarioModel> comentario { get; set; }
    }
}