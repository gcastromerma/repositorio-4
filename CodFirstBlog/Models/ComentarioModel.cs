﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace CodFirstBlog.Models
{
    public class ComentarioModel
    {
        public int id { get; set; }
        public string  contenido { get; set; }
        public string  autor { get; set; }
        public int BlogPostModelID { get; set; }

        [ForeignKey("BlogPostModelID")]
        public BlogPostModel blogPost { get; set; }
    }
}