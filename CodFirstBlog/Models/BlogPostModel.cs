﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace CodFirstBlog.Models
{
    public class BlogPostModel
    {
        public int Id { get; set; }
        [Required]
        [MaxLength(200)]
        public string  Titulo { get; set; }
        [Required]
        public string Contenido { get; set; }
        [Required]
        [MaxLength(50)]
        public string  Autor { get; set; }
        public DateTime Publicacion { get; set; }

        public List<ComentarioModel> comentarios { get; set; }

    }
}