namespace CodFirstBlog.Migrations
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<CodFirstBlog.Models.BlogContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = true;
        }

        protected override void Seed(CodFirstBlog.Models.BlogContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.

            context.comentario.AddOrUpdate(x => x.id, new Models.ComentarioModel() {
                id = 1,
                autor = "JUan Linares",
                BlogPostModelID = 1,
                contenido = "ejemplo de contenido"
            });
        }
    }
}
