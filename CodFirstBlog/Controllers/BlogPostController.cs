﻿using CodFirstBlog.Models;
using CodFirstBlog.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace CodFirstBlog.Controllers
{
    public class BlogPostController : Controller
    {
        #region Constructor
        /// <summary>
        /// creo una variable tipo repository y un constructor
        /// </summary>
        BlogPostRepository _repo;
        public BlogPostController()
        {
            _repo = new BlogPostRepository();
        }
        #endregion
        
        // GET: BlogPost
        public ActionResult Index()
        {
            var resp = _repo.ObtenerTodos();
            var comentario = resp[0].comentarios[0];
            return View(resp);
        }

        // GET: BlogPost/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }
       
        #region Crea nuevo >BLog
        // GET: BlogPost/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: BlogPost/Create
        [HttpPost]
        public ActionResult Create(BlogPostModel obj)
        {
            try
            {
                if (ModelState.IsValid)
                {
                    _repo.crearBlog(obj);
                    return RedirectToAction("Index");
                }               
            }
            catch
            {
               
            }
           return View(obj); 
        }
        #endregion

        // GET: BlogPost/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: BlogPost/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: BlogPost/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: BlogPost/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
